# All unpinned files are extracted from redfin TQ3A.230805.001

# Apps
product/priv-app/Phonesky/Phonesky.apk;PRESIGNED
product/priv-app/PrebuiltGmsCore/m/independent/AndroidPlatformServices.apk;PRESIGNED
product/priv-app/PrebuiltGmsCore/PrebuiltGmsCoreSc.apk;OVERRIDES=NetworkRecommendation;PRESIGNED
product/priv-app/WellbeingPrebuilt/WellbeingPrebuilt.apk;PRESIGNED
system_ext/priv-app/GoogleServicesFramework/GoogleServicesFramework.apk;PRESIGNED
product/app/LatinIMEGooglePrebuilt/LatinIMEGooglePrebuilt.apk;OVERRIDES=LatinIME;PRESIGNED
product/priv-app/FilesPrebuilt/FilesPrebuilt.apk;PRESIGNED

# Configuration files
product/etc/default-permissions/default-permissions.xml
product/etc/permissions/privapp-permissions-google-p.xml
product/etc/preferred-apps/google.xml
product/etc/sysconfig/google.xml
system/etc/permissions/privapp-permissions-google.xml
system_ext/etc/permissions/privapp-permissions-google-se.xml

